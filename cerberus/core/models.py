from django.conf import settings
from django.db import models

from simple_history.models import HistoricalRecords


class AuditableMixin(models.Model):
    """An entity whose changes can be tracked."""

    modified_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='+',
        null=True,
        blank=True)
    modified = models.DateTimeField(auto_now=True)
    history = HistoricalRecords(inherit=True)

    class Meta:
        abstract = True
