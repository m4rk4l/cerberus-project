from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext as _

from cerberus.core.models import AuditableMixin


class Profile(AbstractUser, AuditableMixin):
    """"""

    class Meta:
        verbose_name = _("Profile")
        verbose_name_plural = ("Profiles")
