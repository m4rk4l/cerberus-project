"""CerberusProject URL Configuration"""
from django.conf import settings

from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from cerberus.accounts import urls as accounts_urls


urlpatterns = [
    path('', TemplateView.as_view(template_name="homepage.html")),
    path('accounts/', include(accounts_urls)),
]


if settings.DEBUG:
    urlpatterns = urlpatterns + [path('admin/', admin.site.urls), ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns = [
            path("__debug__/", include(debug_toolbar.urls))
        ] + urlpatterns
